################################################################
# FILENAME   : 10fold_crossvalidation.R
# AUTHOR     : Brian Denton <dentonb@mskcc.org>
# DATE       : 04/11/2011
# DESCRIPTION: Simulate logistic regression data and then
#              implement 10-fold cross-validation. Compare
#              the time required on a Windows workstation,
#              the Linux machines, and the Linux machines
#              with parallel processing.
################################################################

ProgramPath <- "H:/Biostatistics/Master seminar/Brian 05.10.2011/Linux/PROGRAMS"
DataPath    <- "H:/Biostatistics/Master seminar/Brian 05.10.2011/Linux/DATA"
OutputPath  <- "H:/Biostatistics/Master seminar/Brian 05.10.2011/Linux/OUTPUT"

setwd( ProgramPath )

source( "makeSimulationData.R" )

# Define funcion to do 10-fold cross-validation
crossValidate <- function( simulated.data, test.group )
{
  training.fit <- glm( LungCancer ~ Age + as.factor(Female) + PackYears + as.factor(Female) * PackYears,
                     data = subset(simulated.data, simulated.data$data.group != test.group), family = binomial )
					 
  test.fit <- predict( training.fit, subset(simulated.data, simulated.data$data.group == test.group), type = "response" )

  compare <- simulated.data$LungCancer[c(as.numeric(names(test.fit)))]

  return( prop.table(table( compare, round(test.fit) )) )
}

# Define function to implement simulation

simulate10Xcv <- function( simulated.data = simulationData, parallel = FALSE )
{

        num.sims <- length( simulated.data )
  
	elapsedValidationTime <- vector( mode = "numeric", length = num.sims )

	for( sim in 1:num.sims )
	{
		#Monitor progress
		print(paste("Simulation: ", sim ))
		flush.console()

                if( parallel == FALSE ){
                
		   startTime <- Sys.time()

		   compare.table.1  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 1 )
		   compare.table.2  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 2 )
		   compare.table.3  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 3 )
		   compare.table.4  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 4 )
		   compare.table.5  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 5 )
		   compare.table.6  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 6 )
		   compare.table.7  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 7 )
		   compare.table.8  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 8 )
		   compare.table.9  <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 9 )
		   compare.table.10 <- crossValidate( simulated.data = simulated.data[[sim]], test.group = 10 )

		   stopTime <- Sys.time()
		
		   elapsedTime <- stopTime - startTime
		   elapsedValidationTime[sim] <- elapsedTime
                }else if( parallel == TRUE ){
                   library(multicore)
                
                   startTime <- Sys.time()

		   compare.table.1.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 1 ) )
		   compare.table.2.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 2 ) )
		   compare.table.3.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 3 ) )
		   compare.table.4.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 4 ) )
		   compare.table.5.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 5 ) )
		   compare.table.6.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 6 ) )
		   compare.table.7.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 7 ) )
		   compare.table.8.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 8 ) )
		   compare.table.9.id  <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 9 ) )
		   compare.table.10.id <- parallel( crossValidate( simulated.data = simulated.data[[sim]], test.group = 10 ) )

                   compare.table.collect <- collect( list( compare.table.1.id, compare.table.2.id, compare.table.3.id,
                                                        compare.table.4.id, compare.table.5.id, compare.table.6.id,
                                                        compare.table.7.id, compare.table.8.id, compare.table.9.id,
                                                        compare.table.10.id ) )

                   compare.table.1 <- compare.table.collect[[1]]
                   compare.table.2 <- compare.table.collect[[2]]
                   compare.table.3 <- compare.table.collect[[3]]
                   compare.table.4 <- compare.table.collect[[4]]
                   compare.table.5 <- compare.table.collect[[5]]
                   compare.table.6 <- compare.table.collect[[6]]
                   compare.table.7 <- compare.table.collect[[7]]
                   compare.table.8 <- compare.table.collect[[8]]
                   compare.table.9 <- compare.table.collect[[9]]
                   compare.table.10 <- compare.table.collect[[10]]

	      	   stopTime <- Sys.time()
		
		   elapsedTime <- stopTime - startTime
		   elapsedValidationTime[sim] <- elapsedTime
              }
	}
	
	return( elapsedValidationTime )
}

setwd( DataPath )

# Create simulation data

#simulationData <- makeSimulationData( N = 1E5, num.sims = 100 )
#save( simulationData, file = "simulationData.RData" )

load( "simulationData.RData" )

Windows.time <- simulate10Xcv( simulated.data = simulationData, parallel = FALSE )
#Linux.time <- simulate10Xcv( simulated.data = simulationData, parallel = FALSE )
#Linux.parallel.time <- simulate10Xcv( simulated.data = simulationData, parallel = TRUE )

save(Windows.time = Windows.time, file = "Windows.time.RData" )
#save(Linux.time = Linux.time, file = "Linux.time.RData" )
#save(Linux.parallel.time = Linux.parallel.time, file = "Linux.parallel.time.RData" )

#END OF FILE
